## Carpeta src

* **/src**: Contiene los diferentes módulos de Python que conforman el proyecto.
  * **\<fichero.py\>**: Contiene las funciones principales
  * **\<fichero_test.py\>**: Contiene funciones de test para probar las funciones del módulo fichero.py

* **/data**: Contiene el dataset o datasets del proyecto
    * **\<dataset1.csv\>**: Archivo con las datos de la UEFA Champions League
    
## Estructura del *dataset*

El dataset está compuesto por 7 columnas, con la siguiente descripción:

* **\<Club>**: de tipo \<str\>, representa el nombre del club
* **\<Nation>**: de tipo \<str\>, representa de que país es el equipo
* **\<Coach>**: de tipo \<str\>, representa el entrenador del equipo 
* **\<Formation>**: de tipo \<str\>, representa la alineación del equipo
* **\<MVP>**: de tipo \<str\>, representa el jugador mas valioso del equipo
* **\<Position>**: de tipo \<str\>, representa la posición en la que quedo el equipo en la UEFA
* **\<Season>**: de tipo \<str\>, representa la temporada en la que jugó el equipo la UEFA

## Tipos implementados

Hemos creado un namedtuple para trabajar con los datos del dataset:

Registro= namedtuple('Registro', 'Club, Nation, Coach, Formation, Mvp, Position, Season')

en la que los tipos de cada uno de los campos es string.

## Funciones implementadas

### \<modulo fichero.py\>
  Entrega 1:
    Bloque 0:
          -lee_uefa(fichero): lee los datos del fichero csv y devuelve una lista de tuplas con los datos del fichero.
  
  Entrega 2:
    Bloque 1:
          -lee_uefa(fichero): Es el del bloque 0
    Bloque 2:
          -lista_elementos(lista): Recibe una lista de tuplas con la informacion del archivo csv. Devuelve una lista con uno de los campos sin repetir.

          -lista_con_3_campos(lista, entrenador): Recibe una lista de tuplas y un parámetro llamado entrenador en el que tendremos que asignarle el nombre de algún entrenador. Devuelve una lista con 3 campos si se cumple una condición.
    Bloque 3:
          -suma_propiedad(lista, entrenador): Recibe una lista de tuplas y un parámetro llamado entrenador en el que tendremos que asignarle el nombre de algún entrenador. Devuelve la suma de cuántas veces el entrenador que hemos introducido ha ganado la uefa.

          -promedio_propiedad(lista, pais): Recibe una lista de tuplas y un parámetro llamado pais en el que tendremos que asignarle el nombre de algún pais. Devuelve el promedio de una propiedad, en este caso pais.
  
  Entrega 3:
    Bloque 4:
          -valor_maximo_campo(lista): Recibe una lista de tuplas. Devuelve el valor máximo del campo 'position', filtrado por 'winner'.
    
    Bloque 5:
          -lista_ordenada_por_el_min_de_un_campo(lista, n): Recibe una lista de tuplas y un parámetro llamado n para obtener el número deseado de registros. Devuelve una lista de registros ordenada por 'season' de menor a mayor.
    
    Bloque 6:
          -diccionario_con_los_registros(lista): Recibe una lista de tuplas. Devuelve un diccionario cuyas claves son los entrenadores y los valores la lista con los nombres de los equipos en los que ha estado el entrenador.

          -diccionario_con_los_registros_filtrados(lista, formacion='unknown'): Recibe una lista de tuplas. Devuelve un diccionario cuyas claves son los entrenadores y los valores la lista con los registros filtrados por 'formation'.

### \<test fichero_test.py\>
  En el módulo de pruebas se han definido las siguientes funciones para probar las distintas funciones del módulo principal (fichero.py).
      -test_lee_uefa() : Sirve para llamar a la función lee_uefa(fichero).

      -mostrar_numerado(): que sirve para mostrar un numerado de las distintas tuplas. (Que de momento es innecesario porque solo se pide dar los 3 primeros registros y los 3 ultimos.)

      -test_lista_elementos(): Sirve para llamar a la función lista_elementos(lista)

      -test_lista_con_3_campos(): Sirve para llamar a la funcion lista_con_3_campos(lista, entrenador). Dentro de esta función tenemos que asignarle un valor al parámetro entrenador.

      -test_suma_propiedad(): Sirve para llamar a la funcion suma_propiedad(lista, entrenador). Dentro de esta funcion tenemos que asignarle un valor al parámetro entrenador.

      -test_promedio_propiedad() : Sirve para llamar a la funcion promedio_propiedad(lista, pais). Dentro de esta funcion tenemos que asignarle un valor al parámetro pais.

      -test_valor_maximo_campo(): Sirve para llamar a la función valor_maximo_campo(lista).

      -test_lista_ordenada_por_el_min_de_un_campo(): Sirve para llamar a la función lista_ordenada_por_el_min_de_un_campo(lista, n). Dentro de esta tendremos que asignarle un valor al parámetro n

      -test_diccionario_con_los_registros(): Sirve para llamar a la función diccionario_con_los_registros(lista).

      -test_diccionario_con_los_registros_filtrados(): Sirve para llamar a la función diccionario_con_los_registros_filtrados(lista, formacion). Dentro de esta podremos cambiarle el valor al parámetro formacion. 
