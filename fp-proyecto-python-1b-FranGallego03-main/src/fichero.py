import csv
from matplotlib import pyplot as plt
from collections import namedtuple, Counter
from math import sqrt

Registro= namedtuple('Registro', 'club nation coach formation mvp position season')

# Entrega 1

def lee_uefa(fichero):
    '''Lee el fichero de UEFA.
       Devuelve una lista de tuplas, donde cada tupla contiene las 7 propiedades(club, nation, coach, formation, mvp, position, season)
       ENTRADA:
        -fichero : el nombre del fichero csv.
       SALIDA:
        - Lista de tuplas con la información del csv. [(str,str,str,str,str,str,str)]
    '''
    lista = []

    with open(fichero, encoding='utf-8') as f:

        lector = csv.reader(f, delimiter =',')
        next(lector)

        for club, nation, coach, formation, mvp, position, season in lector:

            tupla = Registro(club.strip(), nation.strip(), coach.strip(), formation.strip("'"), mvp, position, season.strip("'"))

            lista.append(tupla)
    return lista

# Entrega 2

def lista_elementos(lista):
    '''Obtiene una lista de uno de los campos sin repetir
       ENTRADA:
        -lista: Lista de tuplas con la información del csv. [(str,str,str,str,str,str,str)]
       SALIDA:
        -Devuelve una lista con todos los nombres de los equipos que han participado en la Uefa.
    '''
    p =  set([l.club for l in lista])
    p = list(p)
    p.sort()
    return p


def lista_con_3_campos(lista, entrenador):
    '''Obtiene una lista de tuplas con 3 campos
        ENTRADA:
         -lista: Lista de tuplas con la información del csv. [(str,str,str,str,str,str,str)]
         -entrenador: Parámetro en el que tenemos que introducir el nombre del entrenador
        SALIDA:
         -Devuelve una lista de tuplas con 3 campos filtrado por el parámetro entrenador
    '''
    return list([(l.coach, l.club, l.season)for l in lista if l.coach== entrenador])


def suma_propiedad(lista, entrenador):
    '''Obtiene la suma de una propiedad
        ENTRADA:
         -lista= Lista de tuplas con la información del csv. [(str,str,str,str,str,str,str)]
         -entrenador: Parámetro en el que tenemos que introducir el nombre del entrenador
        SALIDA:
         -Devuelve la suma de un propiedad, en este caso coach
    '''
    var1= set([(l.coach, l.season) for l in lista if l.position =='winner' and l.coach == entrenador])    
    var1 = list(var1)
    var1.sort(key= lambda x: x[1])
    return var1
    

def promedio_propiedad(lista, pais):
    '''Obtiene el promedio de una propiedad
        ENTRADA:
         -lista: Lista de tuplas con la información del csv. [(str,str,str,str,str,str,str)]
         -pais: Parámetro en el que tenemos que introducir el nombre del pais.
        SALIDA:
         -Devuelve el promedio de una propiedad, en este caso nation
    '''
    return [l for l in lista if l.nation == pais]

# Entrega 3

def valor_maximo_campo(lista):
    '''Obtiene el registro de uno de los campos, filtrado por 'winner'
        ENTRADA:
         -lista: Lista de tuplas con la información del csv. [(str,str,str,str,str,str,str)]
        SALIDA:
         -Devuelve el valor máximo del campo position
    '''
    res =  [l for l in lista if l.position == "winner"]
    return max(res, key= lambda x: x.season)


def lista_ordenada_por_el_min_de_un_campo(lista, n):
    '''Obtiene una lista de registros ordenados por n registros con menor valor en un campo
        ENTRADA:
         -lista: Lista de tuplas con la información del csv. [(str,str,str,str,str,str,str)]
         -n: Parámetro para obtener el número registros deseados.
        SALIDA:
         -Lista con los registros ordenados por el menor valor del campo season.
    '''
    res = [l for l in lista]
    return sorted(res, key=lambda x: x.season, reverse= False)[:n]


def diccionario_con_los_registros(lista):
    '''Obtiene un diccionario
        ENTRADA:
         -lista: Lista de tuplas con la información del csv. [(str,str,str,str,str,str,str)]
        SALIDA:
         -Diccionario cuyas claves son los entrenadores y los valores la lista con los nombres de los equipos en 
         los que ha estado el entrenador
    '''
    diccionario = dict()
    for l in lista:
        if l.coach in diccionario:
            diccionario[l.coach] += [l.club]
        else:
            diccionario[l.coach] = [l.club]
    return diccionario


def diccionario_con_los_registros_filtrados(lista, formacion='unknown'):
    '''Obtiene un diccionario con registros que cumplan determinada propiedad.
        ENTRADA:
         -lista: Lista de tuplas con la información del csv. [(str,str,str,str,str,str,str)]
         -formacion: Parámetro para filtrar los registros
        SALIDA:
         - Diccionario cuyas claves son los entrenadores y los valores la lista con los registros filtrados 
         por 'formation'.
    '''
    diccionario = dict()
    for l in lista:
        if l.coach in diccionario:
            if l.formation != formacion:
                diccionario[l.coach] += [l]
        else:
            if l.formation != formacion:
                diccionario[l.coach] = [l]
    return diccionario








