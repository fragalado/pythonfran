from fichero import * 

def mostrar_numerado(coleccion):
    i = 0
    for p in coleccion:
        i +=1
        print(i, p)

def test_lee_uefa():
    print('='*25,'test_lee_uefa'+'='*25)
    print('Leidos',len(uefa), 'datos de la uefa')
    print(' ')
    print('Los tres primeros registros son: ',uefa[:3] )
    print(' ')  
    print('Los tres ultimos registros son: ',uefa[-3:])
    print(' ')
    #mostrar_numerado(uefa)
    # Si se desea ver el registro completo solo hay que quitar el hashtag de delante de 'mostrar_numerado(uefa)'


def test_lista_elemento():
    print('='*25,'test_lista_elemento','='*25)
    print()

    lista_elemento = lista_elementos(uefa)
    print('Lista filtrada por todos los clubs: ',lista_elemento)
    print()


def test_lista_con_3_campos():
    print('='*25,'test_lista_con_3_campos','='*25)
    print()

    entrenador = 'Robert Paisley'
    print(f'Lista filtrada por {entrenador}: ',lista_con_3_campos(uefa, entrenador))
    print()


def test_suma_propiedad():
    print('='*25, 'test_suma_propiedad','='*25)
    print()

    entrenador = 'Zinédine Zidane'
    entrenador1 = suma_propiedad(uefa, entrenador)
    print (f'El entrenador {entrenador} ha ganado la uefa {len(entrenador1)} veces, estos son los años:')
    print()
    for i in entrenador1:
        print(i)
    print()


def test_promedio_propiedad():
    print('='*25, 'test_promedio_propiedad','='*25)
    print(' ')

    pais = 'Germany'
    pais1 = promedio_propiedad(uefa, pais)
    porcentaje = (len(pais1)/128) *100
    print(f'El pais {pais} tiene un porcentaje de aparición en la uefa de {porcentaje:.2f}%')
    print()


def test_valor_maximo_campo():
    print('='*25, 'test_valor_maximo_campo','='*25)
    print()

    res = valor_maximo_campo(uefa)
    print(res)
    print()


def test_lista_ordenada_por_el_min_de_un_campo():
    print('='*25,'test_lista_ordenada_por_el_min_de_un_campo','='*25)
    print()

    res = lista_ordenada_por_el_min_de_un_campo(uefa, 4)
    print(res)
    print()


def test_diccionario_con_los_registros():
    print('='*25,'test_diccionario_con_los_registros','='*25)
    print()

    res = diccionario_con_los_registros(uefa)
    print(res)
    print()


def test_diccionario_con_los_registros_filtrados():
    print('='*25,'test_diccionario_con_los_registros_filtrados','='*25)
    print()

    res = diccionario_con_los_registros_filtrados(uefa)
    print(res)
    print()


if __name__ == '__main__':    # Con esto conseguimos poder ejecutar lo que nosotros queramos
    uefa = lee_uefa('./data/UEFA.csv')


    test_lee_uefa()

    # ENTREGA 2:
    #test_lista_elemento()
    #test_lista_con_3_campos()
    #test_suma_propiedad()
    #test_promedio_propiedad()

    # ENTREGA 3:
    test_valor_maximo_campo()
    test_lista_ordenada_por_el_min_de_un_campo()
    test_diccionario_con_los_registros()
    test_diccionario_con_los_registros_filtrados()
    